﻿using UnityEngine;
using System.Collections;

public class Skin2d : MonoBehaviour
{

    public GameObject root;
    public float angle = 90;
    public float left_right = 10;
    public float up_down = 4;

    public float mass = 0.7f;

    public Vector3 someV;
    private Vector3[] lastRot;
    public SkeletonWrapper sw;

    private Quaternion[] lastQ;

    private float[] ang;

    private float[] startAng;

    public GameObject HipCenter;
    public GameObject Spine;
    public GameObject ShoulderCenter;
    public GameObject Head;
    public GameObject ShoulderLeft;
    public GameObject ElbowLeft;
    public GameObject WristLeft;
    public GameObject HandLeft;
    public GameObject ShoulderRight;
    public GameObject ElbowRight;
    public GameObject WristRight;
    public GameObject HandRight;
    public GameObject HipLeft;
    public GameObject KneeLeft;
    public GameObject AnkleLeft;
    public GameObject FootLeft;
    public GameObject HipRight;
    public GameObject KneeRight;
    public GameObject AnkleRight;
    public GameObject FootRight;

    public int player = 0;
    public float lerpValue = 0.1f;
    GameObject[] bones;

    public float deltaX;
    public float deltaY;
    public float deltaZ;


    public float someVVVV = 180;

    // Use this for initialization
    void Start()
    {
        bones = new GameObject[(int)Kinect.NuiSkeletonPositionIndex.Count] {
        HipCenter ,
        Spine,
        ShoulderCenter,
        Head ,
        ShoulderLeft,
        ElbowLeft ,
        WristLeft ,
        HandLeft ,
        ShoulderRight,
        ElbowRight ,
        WristRight ,
        HandRight ,
        HipLeft ,
        KneeLeft ,
        AnkleLeft ,
        FootLeft ,
        HipRight ,
        KneeRight,
        AnkleRight,
        FootRight,
        }
       ;
        lastRot = new Vector3[bones.Length];
        lastQ = new Quaternion[bones.Length];
        ang = new float[bones.Length];
        startAng = new float[bones.Length];

        for (int i = 0; i < bones.Length; i++)
        {
            startAng[i] = bones[i].transform.eulerAngles.z;
        }


        /*  count = 20


        HipCenter = 0,
        Spine = 1,
        ShoulderCenter = 2,
        Head = 3,
        ShoulderLeft = 4,
        ElbowLeft = 5,
        WristLeft = 6,
        HandLeft = 7,
        ShoulderRight = 8,
        ElbowRight = 9,
        WristRight = 10,
        HandRight = 11,
        HipLeft = 12,
        KneeLeft = 13,
        AnkleLeft = 14,
        FootLeft = 15,
        HipRight = 16,
        KneeRight = 17,
        AnkleRight = 18,
        FootRight = 19,
        */

    }
    void Rotate(int i)
    {
        lastRot[i] = bones[i].transform.localEulerAngles;
        lastQ[i] = bones[i].transform.localRotation;
        if (bones[i] == null)
        {
            return;
        }

        if (sw.boneState[player, i] == Kinect.NuiSkeletonPositionTrackingState.NotTracked)
        {
            return;
        }

        //righ legs
        if (i == 16 || i == 17 || i == 18 //|| i == 19
            )
        {
            R(i, 0);

        }
        //right arms
        if (i == 8 || i == 9 || i == 10// || i == 11
            )
        {

            R(i, 1);
        }
        //left legs
        if (i == 12 || i == 13 || i == 14// || i == 15
            )
        {
            R(i, 2);


        }
        //left arms
        if (i == 4 || i == 5 || i == 6// || i == 7
            )
        {
            R(i, 3);

        }
        //spine line
        if (i == 0 || i == 1 || i == 2// || i == 3
            )
        {
            R(i, 4);
        }

    }

    void SecondRotation(int i)
    {
        //bones[i].transform.rotation = Quaternion.Lerp(bones[i].transform.rotation, lastRot[i], lerpValue);

        //Debug.Log(bones[i].transform.localEulerAngles + " - " + bones[i].transform.eulerAngles);

        //bones[i].transform.localEulerAngles = new Vector3(0, 0, (bones[i].transform.localEulerAngles.z + lastRot[i].z) * lerpValue);


        //bones[i].transform.localEulerAngles = Vector3.Lerp(bones[i].transform.localEulerAngles, lastRot[i], lerpValue);

        //bones[i].transform.localRotation = Quaternion.Slerp(lastQ[i], bones[i].transform.localRotation, lerpValue);

        if (i % 4 == 0)
        {
            if (i/4!=0)
            {
                float a = ( - 80) % 360;
                float b = ( + 80) % 360;
                if (a < 0)
                {
                    a = 360 + a;
                }
                if (a > b)
                {
                    float c = a;
                    a = b; b = c;
                }

               

                if (i==8)
                {
                    //Debug.Log  (bones[i].transform.localEulerAngles.z +";"+a +";"+b);
                }

                //bones[i].transform.localEulerAngles = -new Vector3(0, 0, Mathf.Clamp(bones[i].transform.eulerAngles.z, a, b));

            }
        }
        /*else
        {
            if (i > 4)
            {

                float a = (startAng[i]) % 360;
                float b = (startAng[i] + 90) % 360;
                if (a > b)
                {
                    float c = a;
                    a = b; b = c;
                }

                bones[i].transform.localEulerAngles = new Vector3(0, 0, Mathf.Clamp(bones[i].transform.eulerAngles.z, a, b));

            }
        }*/





        //bones[i].transform.localRotation = Quaternion.Slerp(lastQ[i], bones[i].transform.localRotation, lerpValue);
    }



    Vector2 Get2D(Vector3 v3)
    {
        return new Vector2(v3.x, v3.y);
    }

    void R(int i, int left)
    {
        Vector2 dir = Get2D(sw.rawBonePos[player, i + 1]) - Get2D(sw.rawBonePos[player, i]);

        switch (left)
        {
            case 0:
                {

                    /*if (-dir.y>0)
                    {
                        break;

                    }*/
                    dir = new Vector2(-dir.x, dir.y);
                    float oneD = Mathf.PI / 360;
                    float value = Mathf.Asin(dir.x / dir.magnitude) / oneD;


                    //16
                    float endV = 0;
                    if (i == 16)
                    {

                        endV = Mathf.Clamp(value * mass, -someVVVV, someVVVV);
                        ang[i] = value * mass;

                    }
                    else
                    {
                        endV = Mathf.Clamp(value * mass + ang[i - 1], -someVVVV, someVVVV);

                        ang[i] = endV;

                        endV -= +ang[i - 1];
                    }

                    bones[i].transform.rotation = Quaternion.Euler(0, 180, value * mass + 180);

                    break;
                }
            case 1:
                {

                    /*if (-dir.x > 0)
                    {
                        break;
                    }*/

                    float oneD = Mathf.PI / 360;
                    float value = Mathf.Asin(dir.y / dir.magnitude) / oneD;

                    //8
                    float endV = 0;
                    if (i == 8)
                    {

                        endV = Mathf.Clamp(value * mass, -someVVVV, someVVVV);
                        ang[i] = value * mass;

                    }


                    bones[i].transform.rotation = Quaternion.Euler(0, 0, value * mass - 90);

                    break;
                }
            case 2:
                {

                    /*if (-dir.y > 0)
                    {
                        break;
                    }*/

                    dir = new Vector2(-dir.x, dir.y);
                    //dir = -dir;
                    float oneD = Mathf.PI / 360;
                    float value = Mathf.Asin(dir.x / dir.magnitude) / oneD;

                    //12
                    float endV = 0;
                    if (i == 12)
                    {

                        endV = Mathf.Clamp(value * mass, -someVVVV, someVVVV);
                        ang[i] = value * mass;

                    }
                    else
                    {
                        endV = Mathf.Clamp(value * mass + ang[i - 1], -someVVVV, someVVVV);

                        ang[i] = endV;

                        endV -= +ang[i - 1];
                    }

                    bones[i].transform.rotation = Quaternion.Euler(0, 180, value * mass + 180);

                    break;

                }
            case 3:
                {
                    /*if (-dir.x < 0)
                    {
                        break;
                    }*/



                    dir = -dir;
                    float oneD = Mathf.PI / 360;
                    float value = Mathf.Asin(dir.y / dir.magnitude) / oneD;

                    //4
                    float endV = 0;
                    if (i == 4)
                    {

                        endV = Mathf.Clamp(value * mass, -someVVVV, someVVVV);
                        ang[i] = value * mass;

                    }
                    else
                    {
                        endV = Mathf.Clamp(value * mass + ang[i - 1], -someVVVV, someVVVV);

                        ang[i] = endV;

                        endV -= +ang[i - 1];
                    }

                    bones[i].transform.rotation = Quaternion.Euler(0, 0, value * mass + 90);

                    break;
                }
            default:
                {
                    //dir = -dir;
                    float oneD = Mathf.PI / 360;
                    float value = Mathf.Asin(dir.x / dir.magnitude) / oneD;

                    bones[i].transform.rotation = Quaternion.Euler(0, 0, -value * mass);



                    break;
                }

        }

    }


    // Update is called once per frame
    void Update()
    {


        for (int i = 0; i < bones.Length; i++)
        {
            if (bones[i] != null)
            {
                Rotate(i);
                Vector3 v3 = new Vector3(sw.bonePos[player, 0].x * left_right//left -right
                   , sw.bonePos[player, 0].y * up_down, // jump
                     sw.bonePos[player, 0].z); // forward -sw.bonePos[player, 0].z

                if (root != null)
                {
                    root.transform.position = v3 + new Vector3(deltaX, deltaY, deltaZ);
                }

            }

        }


        for (int i = 0; i < bones.Length; i++)
        {
            if (bones[i] != null)
            {
                SecondRotation(i);
            }

        }
    }
}
