﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShapesController : MonoBehaviour {

    List<Shape> shapes = new List<Shape>();
    public float armsSpan = 10f;
    GameParams gameParams = new GameParams();
    float checkTimer;
    float delayTimer;


    public Camera cam;
    ShapePool shapePool;
    GameSession gameSession;
    Vector3 middleTop;
    Vector3 middleBottom;

    public LevelUpText levelUptext;
    int currentLevel = 1;

	void Start () {
        gameParams = gameParams.Load();
        shapePool = ShapePool.Instance;
        Sprite sprite = shapePool.prefabs[0].GetComponent<SpriteRenderer>().sprite;
        gameParams.shapeSize = sprite.bounds.max.x - sprite.bounds.min.x;
        gameParams.minWidth = gameParams.minWidth * armsSpan;
        gameParams.maxWidth = gameParams.maxWidth * armsSpan;
        gameParams.width = gameParams.width * armsSpan;
        delayTimer = gameParams.delay;
        checkTimer = gameParams.checkTime;
        TotalTimeShow.Instance.SetTime((int)gameParams.gameTime+1);
        gameSession = GameSessionController.Instance.gameSession;
        //Debug.Log(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1f,0f)));
        //middleTop = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1f,0f));
        //middleBottom = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0f,0f));
        middleTop = cam.ViewportToWorldPoint(new Vector3(0.5f, 1f, 0f));
        middleBottom = cam.ViewportToWorldPoint(new Vector3(0.5f, 0f, 0f));
    }
	
	// Update is called once per frame
	void Update () {
        MoveAllShapes();
        gameParams.gameTime -= Time.deltaTime;
        checkTimer -= Time.deltaTime;
        delayTimer -= Time.deltaTime;
        TotalTimeShow.Instance.SetTime((int)gameParams.gameTime + 1);
        if (gameParams.gameTime < 0)
            Application.Quit();
        CheckParams();
        SpawnNewShapes();
	}

    public void MoveAllShapes()
    {
        
        for (int i = shapes.Count-1; i >= 0; i--)
        {
            if (!shapes[i].Move(middleBottom.y))
            {
                if (!shapes[i].WasHitted())
                    gameSession.AddTotal(1);
                shapePool.PutToPool(shapes[i].gameObject);
                shapes.Remove(shapes[i]);
            }
        }
    }

    public void SpawnNewShapes()
    {
        if (delayTimer < 0f)
        {
            delayTimer = gameParams.delay;
            int count = gameParams.quantity;
            float partWidth = gameParams.width / gameParams.quantity;
            for (int i = 0; i < count; i++)
            {
                Shape shape = shapePool.GetFromPool().GetComponent<Shape>();
                shapes.Add(shape);
                shape.transform.position = middleTop + new Vector3(UnityEngine.Random.Range(partWidth * i - gameParams.width / 2f + gameParams.shapeSize / 2f, partWidth * (i + 1) - gameParams.width / 2f - gameParams.shapeSize), 0f, 10f);
                shape.Reset(gameParams.speed);
            }
        }
    }

    public void CheckParams()
    {
        if (checkTimer < 0f)
        {
            if (gameSession.GetPerformance() > gameParams.lvlUpPerformance)
            {
                currentLevel++;
                levelUptext.SetLevel(currentLevel);
                MusicController.Instance.LevelUp();

                float temp = gameParams.speed;
                gameParams.speed = Mathf.Clamp((1f + gameParams.speedUpDelta) * gameParams.speed, gameParams.fallingSpeedMin, gameParams.fallingSpeedMax);
                if (temp != gameParams.speed)
                    gameSession.AddChange("Speed", temp, gameParams.speed, DateTime.Now.ToString());

                temp = gameParams.width;
                gameParams.width = Mathf.Clamp(gameParams.width + (gameParams.widthUpDelta) * armsSpan, gameParams.minWidth, gameParams.maxWidth);
                if (temp != gameParams.width)
                    gameSession.AddChange("Width", temp, gameParams.width, DateTime.Now.ToString());

                temp = gameParams.delay;
                gameParams.delay = Mathf.Clamp((1f+gameParams.delayUpDelta) * gameParams.delay, gameParams.minDelay, gameParams.maxDelay);
                if (temp != gameParams.delay)
                    gameSession.AddChange("Delay", temp * 1000, gameParams.delay * 1000, DateTime.Now.ToString());

                temp = gameParams.checkTime;
                gameParams.checkTime = Mathf.Clamp(gameParams.lvlUpCheckTime, gameParams.minCheckTime, gameParams.maxCheckTime);
                if (temp != gameParams.checkTime)
                    gameSession.AddChange("CheckTime", temp, gameParams.checkTime, DateTime.Now.ToString());

                temp = gameParams.quantity;
                gameParams.quantity = Mathf.Clamp((int)(temp + gameParams.quantityUpDelta), gameParams.minQuantity, gameParams.maxQuantity);
                 if (temp != gameParams.quantity)
                     gameSession.AddChange("Quantity", temp, gameParams.quantity, DateTime.Now.ToString());
            }

            if (gameSession.GetPerformance() < gameParams.lvlDownPerformance)
            {
               
                float temp = gameParams.speed;
                gameParams.speed = Mathf.Clamp((1f + gameParams.speedDownDelta)*gameParams.speed, gameParams.fallingSpeedMin, gameParams.fallingSpeedMax);
                if (temp != gameParams.speed)
                    gameSession.AddChange("Speed", temp, gameParams.speed, DateTime.Now.ToString());

                temp = gameParams.width;
                gameParams.width = Mathf.Clamp(gameParams.width + gameParams.widthDownDelta* armsSpan, gameParams.minWidth, gameParams.maxWidth);
                if (temp != gameParams.width)
                    gameSession.AddChange("Width", temp, gameParams.width, DateTime.Now.ToString());

                temp = gameParams.delay;
                gameParams.delay = Mathf.Clamp((1f + gameParams.delayDownDelta)*gameParams.delay, gameParams.minDelay, gameParams.maxDelay);
                if (temp != gameParams.delay)
                    gameSession.AddChange("Delay", temp * 1000, gameParams.delay * 1000, DateTime.Now.ToString());

                temp = gameParams.checkTime;
                gameParams.checkTime = Mathf.Clamp(gameParams.lvlDownCheckTime, gameParams.minCheckTime, gameParams.maxCheckTime);
                if (temp != gameParams.checkTime)
                    gameSession.AddChange("CheckTime", temp, gameParams.checkTime, DateTime.Now.ToString());

                temp = gameParams.quantity;
                gameParams.quantity = Mathf.Clamp((int)(temp + gameParams.quantityDownDelta), gameParams.minQuantity, gameParams.maxQuantity);
                if (temp != gameParams.quantity)
                    gameSession.AddChange("Quantity", temp, gameParams.quantity, DateTime.Now.ToString());
            }
            checkTimer = gameParams.checkTime;
        }
    }
}
