﻿using UnityEngine;
using System.Collections;

public class Shape : MonoBehaviour
{
    bool legHit = false;
    bool armHit = false;
    float speed;
    public Color defaultColor;
    public Color oneHitColor;
    public Color secondHitColor;
    public GameObject plusPrefab;
    float groundY;
    SpriteRenderer spriteRenderer;
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        Color a = spriteRenderer.color;
        spriteRenderer.color = defaultColor;
        groundY = GameObject.Find("ground").transform.position.y;
    }

    void Update()
    {
        float alphaValue = 0.5f;
        if (transform.position.y - groundY + 0.1f < alphaValue)
        {
            Color a = spriteRenderer.color;
            spriteRenderer.color = new Color(a.r, a.g, a.b, Mathf.Clamp((transform.position.y - groundY + 0.1f) / alphaValue, 0, 1));
        }
    }

    public void LegHit(string joint, float x, float y, float z)
    {
        if (!legHit)
        {
            legHit = true;
            this.GetComponent<Animator>().SetTrigger("boom");

            GameSessionController.Instance.AddHit(joint, x, y, z, armHit ? 1 : 0);
            GameObject plus = Instantiate(plusPrefab, transform.position, plusPrefab.transform.rotation) as GameObject;
            SpriteRenderer sr = plus.GetComponent<SpriteRenderer>();
            if (!armHit)
            {            
                MusicController.Instance.LegHit();
                GameSessionController.Instance.AddScore(1);
                GameSessionController.Instance.AddTotal(1);
                TotalScorCounter.Instance.AddScore(5);
                Color a = spriteRenderer.color;
                sr.color = spriteRenderer.color = new Color(oneHitColor.r, oneHitColor.g, oneHitColor.b, a.a);
            }
            else
            {
                MusicController.Instance.BothHit();
                Color a = spriteRenderer.color;
                sr.color = spriteRenderer.color = new Color(secondHitColor.r, secondHitColor.g, secondHitColor.b, a.a);
            }

        }
    }

    public void ArmHit(string joint, float x, float y, float z)
    {
        if (!armHit)
        {
            MusicController.Instance.ArmHit();
            armHit = true;
            GameSessionController.Instance.AddHit(joint, x, y, z, 0);
            GameObject plus = Instantiate(plusPrefab, transform.position, plusPrefab.transform.rotation) as GameObject;
            SpriteRenderer sr = plus.GetComponent<SpriteRenderer>();
            Color a = spriteRenderer.color;
            sr.color = spriteRenderer.color = new Color(oneHitColor.r, oneHitColor.g, oneHitColor.b, a.a);
            if (!legHit)
            {
                GameSessionController.Instance.AddTotal(1);
                GameSessionController.Instance.AddScore(1);
                TotalScorCounter.Instance.AddScore(5);
            }
        }
    }

    public void LegHit(string joint, Vector3 position)
    {
        LegHit(joint, position.x, position.y, position.z);
    }

    public void ArmHit(string joint, Vector3 position)
    {
        ArmHit(joint, position.x, position.y, position.z);
    }

    public bool Move(float border)
    {
        transform.position += Vector3.down * speed / 100f * Time.deltaTime;
        if (transform.position.y < border)
            return false;
        return true;
    }

    public void Reset(float speed_)
    {
        armHit = false;
        legHit = false;
        speed = speed_;
        if (spriteRenderer != null)
            spriteRenderer.color = defaultColor;
       // this.GetComponent<Animator>().SetTrigger("normal");
    }

    public bool WasHitted()
    {
        return armHit || legHit;
    }
}
