﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System;
[Serializable]
public class Parameter {
    [XmlAttribute("ParameterName")]
    public string name;
    [XmlAttribute("OldValue")]
    public float oldValue;
    [XmlAttribute("NewValue")]
    public float newValue;
    [XmlAttribute("TimeStamp")]
    public string timeStamp;

    public Parameter()
    { 
    }

    public Parameter(string name_, float oldValue_, float newValue_, string timeStamp_)
    {
        this.name = name_;
        this.oldValue = oldValue_;
        this.newValue = newValue_;
        this.timeStamp = timeStamp_;
    }
}
