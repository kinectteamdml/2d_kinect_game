﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TotalTimeShow : MonoBehaviour {
    private static TotalTimeShow instance_;
    public static TotalTimeShow Instance
    {
        get
        {
            instance_ = GameObject.FindObjectOfType<TotalTimeShow>();
            return instance_;
        }
    }

    Text text;
    void Start()
    {
        text = GetComponent<Text>();
    }

    public void SetTime(int time)
    {
        text.text = "Time: " + time;
    }
}
