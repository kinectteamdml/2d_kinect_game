﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TotalScorCounter : MonoBehaviour {
    private static TotalScorCounter instance_;
    public static TotalScorCounter Instance {
        get {
            instance_ = GameObject.FindObjectOfType<TotalScorCounter>();
            return instance_;
        }
    }
    int score = 0;
    Text text;
	void Start () {
        text = GetComponent<Text>();
        text.text = score.ToString();
	}

    public void AddScore(int delta)
    {
        score += delta;
        text.text = score.ToString();
        GameSessionController.Instance.gameSession.SetFinalScore(score);
    }
}
